package ru.t1consulting.vmironova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.api.endpoint.ITaskEndpoint;
import ru.t1consulting.vmironova.tm.api.service.IProjectTaskService;
import ru.t1consulting.vmironova.tm.api.service.IServiceLocator;
import ru.t1consulting.vmironova.tm.api.service.ITaskService;
import ru.t1consulting.vmironova.tm.dto.request.*;
import ru.t1consulting.vmironova.tm.dto.response.*;
import ru.t1consulting.vmironova.tm.enumerated.Sort;
import ru.t1consulting.vmironova.tm.enumerated.Status;
import ru.t1consulting.vmironova.tm.exception.EndpointException;
import ru.t1consulting.vmironova.tm.exception.entity.TaskNotFoundException;
import ru.t1consulting.vmironova.tm.model.Session;
import ru.t1consulting.vmironova.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.t1consulting.vmironova.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    private ITaskService getTaskService() {
        return this.getServiceLocator().getTaskService();
    }

    private IProjectTaskService getProjectTaskService() {
        return this.getServiceLocator().getProjectTaskService();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskBindToProjectResponse bindToProjectTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskBindToProjectRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        try {
            getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskBindToProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIdResponse changeStatusByIdTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        try {
            getTaskService().changeTaskStatusById(userId, id, status);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskChangeStatusByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIndexResponse changeStatusByIndexTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Status status = request.getStatus();
        try {
            getTaskService().changeTaskStatusByIndex(userId, index, status);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskChangeStatusByIndexResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskClearRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        try {
            getTaskService().clear(userId);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCompleteByIdResponse completeByIdTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCompleteByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        try {
            getTaskService().changeTaskStatusById(userId, id, Status.COMPLETED);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskCompleteByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCompleteByIndexResponse completeByIndexTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCompleteByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        try {
            getTaskService().changeTaskStatusByIndex(userId, index, Status.COMPLETED);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskCompleteByIndexResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCreateRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        try {
            @NotNull final Task task = getTaskService().create(userId, name, description);
            return new TaskCreateResponse(task);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskListResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Sort sort = request.getSort();
        try {
            @NotNull final List<Task> tasks = getTaskService().findAll(userId, sort);
            return new TaskListResponse(tasks);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIdResponse removeByIdTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Task task;
        try {
            task = getTaskService().findOneById(userId, id);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        if (task == null) throw new TaskNotFoundException();
        try {
            getTaskService().removeById(userId, task.getId());
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskRemoveByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIndexResponse removeByIndexTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable Task task;
        try {
            task = getTaskService().findOneByIndex(userId, index);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        if (task == null) throw new TaskNotFoundException();
        try {
            getTaskService().removeById(userId, task.getId());
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskRemoveByIndexResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByIdResponse showByIdTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        try {
            @Nullable final Task task = getTaskService().findOneById(userId, id);
            return new TaskShowByIdResponse(task);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByIndexResponse showByIndexTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        try {
            @Nullable final Task task = getTaskService().findOneByIndex(userId, index);
            return new TaskShowByIndexResponse(task);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByProjectIdResponse showByProjectIdTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowByProjectIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        try {
            @NotNull final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
            return new TaskShowByProjectIdResponse(tasks);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskStartByIdResponse startByIdTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskStartByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        try {
            getTaskService().changeTaskStatusById(userId, id, Status.IN_PROGRESS);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskStartByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskStartByIndexResponse startByIndexTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskStartByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        try {
            getTaskService().changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskStartByIndexResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUnbindFromProjectResponse unbindFromProjectTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUnbindFromProjectRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        try {
            getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskUnbindFromProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIdResponse updateByIdTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        try {
            getTaskService().updateById(userId, id, name, description);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskUpdateByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIndexResponse updateByIndexTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        try {
            getTaskService().updateByIndex(userId, index, name, description);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskUpdateByIndexResponse();
    }

}
