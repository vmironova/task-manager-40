package ru.t1consulting.vmironova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.api.repository.ICommandRepository;
import ru.t1consulting.vmironova.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService extends ICommandRepository {

    void add(@Nullable AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByArgument(@Nullable String argument);

    @Nullable
    AbstractCommand getCommandByName(@Nullable final String command);

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

    @NotNull
    Iterable<AbstractCommand> getCommandsWithArgument();

}
